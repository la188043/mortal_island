#include <iostream>
#include <SFML/Graphics.hpp>

using namespace std;

int main()
{
    sf::RenderWindow renderWindow(sf::VideoMode(1200, 800), "Start");
    sf::Event event;

    while (renderWindow.isOpen())
    {
        while (renderWindow.pollEvent(event))
        {
            if (event.type == sf::Event::EventType::Closed)
            {
                renderWindow.close();
            }
        }

        renderWindow.clear();
        renderWindow.display();
    }

    return 0;
}
